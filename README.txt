CONTENTS OF THIS FILE
=====================

* 1. Introduction
* 2. Requirements
* 3. Installation
* 4. How to use
* 5. Maintainers

0. State of this module
=======================

The current state is not production ready. It is to be seen as a proof of
concept. It's in early development stage and not yet covered with tests.

If you want to support the ongoing development,
feel free to contact me (see "5. Maintainers")
or use the issue queue (https://www.drupal.org/project/issues/congruency).

1. Introduction
===============

This module introduces a concept for displaying content pieces
using a congruency approach. The congruency approach here can be described
as follows:

1. There is a given `source` that contains arbitrary field values.
   Within the scope of this approach, only fields that are user-defined will be
   considered, i.e. no base fields, no computed and internal fields.
2. There is a given `target` that contains field values that are all
   equal with a subset of field values of a `source`.
3. Equality of field values means, that both source and target own a field of
   the same name and same field type, and all plain values of the target field
   are also present as plain values of the source field.
4. If (3.) applies to all fields of the target regards the source,
   then source and target are `congruent`.

This approach has been implemented for custom blocks (i.e. block content) as
targets, using contexts which provide entity values as sources. Contexts are
typically known when working with block plugins.

The implementation includes a nested traversal of entity references, in case
the given source itself is not congruent, but entity references in the scoped
fields might be. This nested traversal enables for example the use case
to display a single paragraph (https://www.drupal.org/project/paragraphs) of
a node having multiple paragraphs within an inline block via Layout Builder.

2. Requirements
===============

Required:
This module currently only makes sense when custom blocks are available, i.e.
the block_content module provided by Drupal core must be installed.

Optional:
This module supports congruency settings for Layout Builder. Both inline blocks
and existing custom blocks are supported within Layout Builder.

3. Installation
===============

Install this module as usual.
See https://www.drupal.org/docs/extending-drupal/installing-modules for more
details on how to install a module.

4. How to use
===============

Custom block content can be places as usual via `/admin/structure/block`.
On the configuration form, you should see a new area
for setting up "Congruency". It suggests a list of possible context to be used
as top-level source.

When placing custom blocks with a selected congruency context,
then it works the following way:

- When a context is chosen, the display of the block content will be replaced
  by values of the context item.
  This would only happen though, if the context item is congruent,
  i.e. it contains matching field names and values to the block content.
- When the context item is not congruent by itself, then references
  within its field values will be checked for congruency.
- The first found congruent item will be used.
- As a last resort, the references within the block content's field values
  will be checked. At the end, if no congruent item was found,
  the block content will not display at all.

5. Maintainers
===============

* Maximilian Haupt (mxh) - https://www.drupal.org/u/mxh
