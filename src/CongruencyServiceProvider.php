<?php

namespace Drupal\congruency;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Sets the layout_builder.get_block_dependency_subscriber service definition.
 *
 * This service is dependent on the block_content module so it must be provided
 * dynamically.
 *
 * @internal
 *   Service providers are internal.
 */
class CongruencyServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['layout_builder'])) {
      $definition = new Definition('Drupal\congruency\EventSubscriber\CongruentSectionComponentRenderArray');
      $definition->setArguments([
        new Reference('congruency.entity_view_builder'),
      ]);
      $definition->addTag('event_subscriber');
      $container->setDefinition('congruency.render_block_component_subscriber', $definition);
    }
  }

}
