<?php

namespace Drupal\congruency\EventSubscriber;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\congruency\ViewBuilder\CongruentEntityViewBuilder;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;

/**
 * Builds a render array for a congruent Layout Builder section component.
 */
class CongruentSectionComponentRenderArray implements EventSubscriberInterface {

  /**
   * The view builder for congruent entities.
   *
   * @var \Drupal\congruency\ViewBuilder\CongruentEntityViewBuilder
   */
  protected $viewBuilder;

  /**
   * Event subscriber service constructor.
   *
   * @param \Drupal\congruency\ViewBuilder\CongruentEntityViewBuilder $view_builder
   *   The view builder for congruent entities.
   */
  public function __construct(CongruentEntityViewBuilder $view_builder) {
    $this->viewBuilder = $view_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = [
      'onBuildRender',
      -10,
    ];
    return $events;
  }

  /**
   * Builds render arrays for congruent blocks within a Layout Builder section.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $congruency_settings = $event->getComponent()->getThirdPartySettings('congruency');
    if (empty($congruency_settings) || !isset($congruency_settings['context']) || $congruency_settings['context'] === '_none') {
      return;
    }

    $build = $event->getBuild();
    $plugin = $event->getPlugin();

    $config = NULL;
    if ($plugin instanceof ConfigurableInterface) {
      $config = $plugin->getConfiguration();
    }
    else {
      if (isset($build['#configuration'])) {
        $config = $build['#configuration'];
      }
    }

    if (empty($config['view_mode'])) {
      return;
    }
    $view_mode = $config['view_mode'];

    $content_child_key = NULL;
    $target = NULL;
    foreach (Element::children($build) as $child) {
      foreach (Element::properties($build[$child]) as $property) {
        if ($build[$child][$property] instanceof ContentEntityInterface) {
          $content_child_key = $child;
          $target = $build[$child][$property];
          break;
        }
      }
    }

    if (!isset($content_child_key, $target)) {
      return;
    }

    $contexts = $event->getContexts();
    $build[$content_child_key] = $this->viewBuilder->view($target, $view_mode, $congruency_settings, $contexts);

    $event->setBuild($build);
  }

}
