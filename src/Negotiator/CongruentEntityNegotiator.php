<?php

namespace Drupal\congruency\Negotiator;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * The negotiator service for determining congruent entities.
 */
class CongruentEntityNegotiator {

  /**
   * A list of already known constellations.
   *
   * @var object[]
   */
  protected array $knownConstellations = [];

  /**
   * The maximum nesting level of referenced entities to check for.
   *
   * @var int
   */
  protected int $radius;

  /**
   * Negotiator service constructor.
   *
   * @param int $radius
   *   The maximum nesting level of referenced entities to check for.
   */
  public function __construct(int $radius) {
    $this->radius = $radius;
  }

  /**
   * Get the entity that is a congruent item to the given target.
   *
   * The given source is congruent, when it contains matching field names
   * and values to the given target. When the source is not congruent by itself,
   * then references within its field values will be checked for congruency.
   * The first found congruent item will be used. As a last resort, the
   * references within the target's field values will be checked.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $source
   *   The source entity that might contain values that are congruent.
   * @param \Drupal\Core\Entity\ContentEntityInterface $target
   *   The target entity for which a congruent item should be found.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The congruent entity if found, NULL otherwise.
   */
  public function getCongruentItem(ContentEntityInterface $source, ContentEntityInterface $target) {
    $info = (object) [
      'source_type' => $source->getEntityTypeId(),
      'source_id' => $source->getRevisionId() ?: $target->id(),
      'source_uuid' => $source->uuid(),
      'source_langcode' => $source->language()->getId(),
      'target_type' => $target->getEntityTypeId(),
      'target_id' => $target->getRevisionId() ?: $target->id(),
      'target_uuid' => $target->uuid(),
      'target_langcode' => $target->language()->getId(),
    ];
    foreach ($this->knownConstellations as $constellation) {
      if ($constellation->info == $info) {
        return $constellation->item;
      }
    }
    $item = $this->doGetNestedCongruentItem([$source], $target);
    $this->knownConstellations[] = (object) ['info' => $info, 'item' => $item];
    return $item;
  }

  /**
   * Internal method to get the congruent entity by using nesting levels.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $sources
   *   All source entities of the given nesting level,
   *   that might contain values that are congruent.
   * @param \Drupal\Core\Entity\ContentEntityInterface $target
   *   The target entity for which a congruent item should be found.
   * @param int $nesting_level
   *   The current nesting level. This value must not exceed the radius value.
   * @param mixed[] &$cached
   *   Arbitrary cached items during nested traversal of source data.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The congruent entity if found, NULL otherwise.
   */
  protected function doGetNestedCongruentItem(array $sources, ContentEntityInterface $target, int $nesting_level = 0, array &$cached = []) {
    $target_fields = [];
    $target_referenced_entities = [];
    $this->getNotEmptyFieldsAndReferences($target, $target, $target_fields, $target_referenced_entities, $cached);

    $item = NULL;
    $next_level_entities = [];
    foreach ($sources as $source) {
      if ($this->sourceIsCongruentToTarget($source, $target, $cached)) {
        $item = $source;
        break;
      }
      elseif ($nesting_level < $this->radius) {
        $source_fields = [];
        $source_referenced_entities = [];
        $this->getNotEmptyFieldsAndReferences($source, $target, $source_fields, $source_referenced_entities, $cached);
        foreach ($source_referenced_entities as $referenced_entity) {
          // Check same references existing in both source and target at last.
          if (!in_array($referenced_entity, $target_referenced_entities, TRUE) && !in_array($referenced_entity, $next_level_entities, TRUE)) {
            $next_level_entities[] = $referenced_entity;
          }
        }
      }
    }
    if (!isset($item) && $nesting_level < $this->radius && !empty($next_level_entities)) {
      $item = $this->doGetNestedCongruentItem($next_level_entities, $target, $nesting_level + 1, $cached);
    }
    if (!isset($item) && $nesting_level === 0 && !empty($target_referenced_entities)) {
      $item = $this->doGetNestedCongruentItem($target_referenced_entities, $target, $nesting_level + 1, $cached);
    }
    return $item;
  }

  /**
   * Determines whether the given source is congruent to the given target.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $source
   *   The source entity that might contain values that are congruent.
   * @param \Drupal\Core\Entity\ContentEntityInterface $target
   *   The target entity for which a congruent item should be found.
   * @param mixed[] &$cached
   *   Arbitrary cached items during nested traversal of source data.
   *   This parameter is used for internal nested extraction.
   *
   * @return bool
   *   Returns TRUE if the given source is congruent to the target.
   */
  public function sourceIsCongruentToTarget(ContentEntityInterface $source, ContentEntityInterface $target, array &$cached = []) {
    // Use the same language version, if possible.
    $langcode = $target->language()->getId();
    if ($source->language()->getId() != $langcode && $source->hasTranslation($langcode)) {
      $source = $source->getTranslation($langcode);
    }

    $target_fields_to_check = [];
    $target_referenced_entities = [];
    $this->getNotEmptyFieldsAndReferences($target, $target, $target_fields_to_check, $target_referenced_entities, $cached);

    $congruent_fields_found = 0;
    foreach ($target_fields_to_check as $field_name => $target_field_items) {
      if (!$source->hasField($field_name)) {
        break;
      }
      $source_field_items = $source->get($field_name);
      /** @var \Drupal\Core\Field\FieldItemInterface $target_field_item */
      foreach ($target_field_items as $target_field_item) {
        $congruent_value_found = FALSE;
        $target_field_properties = $target_field_item->getProperties();
        /** @var \Drupal\Core\Field\FieldItemInterface $source_field_item */
        foreach ($source_field_items as $source_field_item) {
          $source_field_properties = $source_field_item->getProperties();
          foreach ($target_field_properties as $property_name => $target_field_property) {
            if (!isset($source_field_properties[$property_name])) {
              break 2;
            }
            $source_field_property = $source_field_properties[$property_name];
            if ($target_field_property->getValue() != $source_field_property->getValue()) {
              break 2;
            }
          }
          $congruent_value_found = TRUE;
          break;
        }
        if (!$congruent_value_found) {
          break 2;
        }
      }
      $congruent_fields_found++;
    }

    $source_is_congruent = $congruent_fields_found > 0 && count($target_fields_to_check) === $congruent_fields_found;
    return $source_is_congruent;
  }

  /**
   * Helper method to get not empty field items and referenced entities.
   *
   * Base fields, computed fields and internal fields are excluded.
   * Only fields that are configurable via settings of both view display
   * and form display are included.
   * Only referenced entities of the included field items are being included.
   * The field items and referenced entities of those fields, that do not exist
   * in the given target entity, are being appended to the end of their list.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the values for.
   * @param \Drupal\Core\Entity\ContentEntityInterface $target
   *   The congruency target entity.
   * @param array &$fields
   *   A reference to the fields array that will be filled.
   * @param array &$referenced_entities
   *   A reference to the entity list that will be filled.
   * @param mixed[] &$cached
   *   Arbitrary cached items during nested traversal of source data.
   */
  protected function getNotEmptyFieldsAndReferences(ContentEntityInterface $entity, ContentEntityInterface $target, array &$fields, array &$referenced_entities, array &$cached = []) {
    if ($entity === $target && isset($cached['target_items'])) {
      $fields += $cached['target_items']['fields'];
      $referenced_entities += $cached['target_items']['referenced_entities'];
      return;
    }

    $base_field_definitions = $entity::baseFieldDefinitions($entity->getEntityType());
    $field_items_to_append = [];
    $references_to_append = [];
    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      if (isset($base_field_definitions[$field_name])) {
        continue;
      }
      if ($field_definition->isComputed() || $field_definition->isInternal()) {
        continue;
      }
      if (!$field_definition->isDisplayConfigurable('view') || !$field_definition->isDisplayConfigurable('form')) {
        continue;
      }

      $field_items = $entity->get($field_name);
      $field_references = [];
      if ($field_items->isEmpty()) {
        continue;
      }
      elseif ($field_items instanceof EntityReferenceFieldItemListInterface) {
        $field_references = $field_items->referencedEntities();
        if (empty($field_references)) {
          continue;
        }
      }

      if ($target->hasField($field_name)) {
        $fields[$field_name] = $field_items;
        $referenced_entities = array_merge($referenced_entities, $field_references);
      }
      else {
        $field_items_to_append[$field_name] = $field_items;
        $references_to_append = array_merge($references_to_append, $field_references);
      }
    }
    $fields += $field_items_to_append;
    $referenced_entities = array_merge($referenced_entities, $references_to_append);
    $referenced_entities = array_filter($referenced_entities, function ($referenced_entity) {
      return $referenced_entity instanceof ContentEntityInterface;
    });

    if ($entity === $target) {
      $cached['target_items'] = [];
      $cached['target_items']['fields'] = $fields;
      $cached['target_items']['referenced_entities'] = $referenced_entities;
    }
  }

}
