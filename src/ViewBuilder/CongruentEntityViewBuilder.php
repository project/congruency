<?php

namespace Drupal\congruency\ViewBuilder;

use Drupal\congruency\Negotiator\CongruentEntityNegotiator;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;

/**
 * The view builder for congruent entities.
 *
 * Use this view builder when an entity is to be defined as congruent,
 * instead of using the regular view builder.
 */
class CongruentEntityViewBuilder {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The congruency negotiator.
   *
   * @var \Drupal\congruency\Negotiator\CongruentEntityNegotiator
   */
  protected $congruencyNegotiator;

  /**
   * A list of already viewed items, mainly used to prevent endless recursion.
   *
   * @var object[]
   */
  protected $viewedItems = [];

  /**
   * View builder service contructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository.
   * @param \Drupal\congruency\Negotiator\CongruentEntityNegotiator $congruency_negotiator
   *   The congruency negotiator.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ContextRepositoryInterface $context_repository, CongruentEntityNegotiator $congruency_negotiator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->contextRepository = $context_repository;
    $this->congruencyNegotiator = $congruency_negotiator;
  }

  /**
   * Builds the render array for a congruent entity of the given target.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $target
   *   The target entity, for which to render a congruent entity instead.
   * @param string $view_mode
   *   The view mode that should be used to render the congruent entity.
   * @param array $congruency_settings
   *   The congruency settings, usually provided as third party settings.
   * @param \Drupal\Component\Plugin\Context\ContextInterface[] $contexts
   *   (optional) An array of available contexts, keyed by context id.
   * @param bool $prevent_recursion
   *   (optional) Whether endless recursion should be prevented.
   *   Default is TRUE.
   * @param bool $access_check
   *   (optional) Whether to include view access check or not. Default is TRUE.
   *
   * @return array
   *   A render array for the congruent entity.
   */
  public function view(ContentEntityInterface $target, $view_mode, array $congruency_settings, array $contexts = [], $prevent_recursion = TRUE, $access_check = TRUE) {
    $cache_data = (new CacheableMetadata())->addCacheableDependency($target);
    $context_id = $congruency_settings['context'];
    if ((!isset($contexts[$context_id]) || !$contexts[$context_id]->hasContextValue()) && substr($context_id, 0, 1) === '@') {
      $contexts = array_merge($contexts, $this->contextRepository->getRuntimeContexts([$context_id]));
    }

    $source = NULL;
    if (isset($contexts[$context_id])) {
      $context = $contexts[$context_id];
      $cache_data->addCacheableDependency($context);
      $source = $context->hasContextValue() ? $context->getContextValue() : NULL;
    }

    $build = [];
    $congruent_entity = !empty($source) ? $this->congruencyNegotiator->getCongruentItem($source, $target) : NULL;
    if (!empty($congruent_entity)) {
      $cache_data->addCacheableDependency($congruent_entity);

      $being_viewed = (object) [
        'target_type' => $target->getEntityTypeId(),
        'target_id' => $target->getRevisionId() ?: $target->id(),
        'target_uuid' => $target->uuid(),
        'target_langcode' => $target->language()->getId(),
        'congruent_type' => $congruent_entity->getEntityTypeId(),
        'congruent_id' => $congruent_entity->getRevisionId() ?: $congruent_entity->id(),
        'congruent_uuid' => $congruent_entity->uuid(),
        'congruent_langcode' => $congruent_entity->language()->getId(),
      ];

      $is_view_allowed = $access_check ? $congruent_entity->access('view') : TRUE;

      $was_already_viewed_before = FALSE;
      foreach ($this->viewedItems as $previously_viewed) {
        if ($previously_viewed == $being_viewed) {
          $is_view_allowed = !$prevent_recursion;
          $was_already_viewed_before = TRUE;
        }
      }

      if ($is_view_allowed) {
        $view_builder = $this->entityTypeManager->getViewBuilder($congruent_entity->getEntityTypeId());
        $build = $view_builder->view($congruent_entity, $view_mode);
        $cache_data->merge(CacheableMetadata::createFromRenderArray($build));
        if (!$was_already_viewed_before) {
          $this->viewedItems[] = $being_viewed;
        }
      }
    }

    $cache_data->applyTo($build);
    return $build;
  }

}
