<?php

namespace Drupal\congruency\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * The form element builder for congruency settings.
 */
class CongruencySettingsFormBuilder {

  use StringTranslationTrait;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The service constructor.
   *
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(ContextRepositoryInterface $context_repository, EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    $this->contextRepository = $context_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Builds up the form element for congruency settings.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string|string[] $get_third_party_settings_target_callback
   *   A callable that returns the current state of the given target.
   *   This service offers static methods for most cases.
   */
  public function buildFormElement(array &$form, FormStateInterface $form_state, $get_third_party_settings_target_callback) {
    $form_state->setTemporaryValue('congruency_get_tps_target_callback', $get_third_party_settings_target_callback);
    if (!($third_party_settings_target = static::getThirdPartySettingsTarget($form_state))) {
      return;
    }

    $options = $this->getAllowedContextOptions($form_state);
    $default_value = $third_party_settings_target->getThirdPartySettings('congruency');

    if (!isset($form['third_party_settings'])) {
      $weight = isset($form['actions']['#weight']) ? $form['actions']['#weight'] : 1001;
      if (isset($form['actions'])) {
        $form['actions']['#weight'] = $weight;
      }
      $form['third_party_settings'] = ['#weight' => $weight - 1];
    }

    $form['third_party_settings']['congruency'] = [
      '#type' => 'details',
      '#title' => $this->t('Congruency'),
      '#open' => TRUE,
      'context' => [
        '#title' => $this->t('Context'),
        '#type' => 'select',
        '#options' => $options,
        '#required' => FALSE,
        '#empty_value' => '_none',
        '#default_value' => isset($default_value['context']) ? $default_value['context'] : NULL,
        '#weight' => 10,
      ],
      'help' => [
        '#type' => 'details',
        '#title' => $this->t('Help'),
        '#description' => $this->t("When a context is chosen, the display of the block content will be replaced by values of the context item. This would only happen though, if the context item is congruent, i.e. it contains matching field names and values to the block content. When the context item is not congruent by itself, then references within its field values will be checked for congruency. The first found congruent item will be used. As a last resort, the references within the block content's field values will be checked. At the end, if no congruent item was found, the block content will not display at all."),
        '#open' => FALSE,
        '#weight' => 100,
      ],
    ];

    $form['#validate'][] = [static::class, 'validateSettingsForm'];

    $submit_callbacks = [];
    if (isset($form['actions']['submit']['#submit'])) {
      $former_submit_callbacks = &$form['actions']['submit']['#submit'];
    }
    else {
      $former_submit_callbacks = &$form['#submit'];
    }
    $i_last_item = count($former_submit_callbacks) - 1;
    foreach ($former_submit_callbacks as $i => $submit_callback) {
      if ($i === $i_last_item) {
        $submit_callbacks[] = [static::class, 'submitSettingsForm'];
      }
      $submit_callbacks[] = $submit_callback;
    }
    $former_submit_callbacks = $submit_callbacks;
  }

  /**
   * Get allowed context options for the given form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The allowed context options.
   */
  public function getAllowedContextOptions(FormStateInterface $form_state) {
    $options = ['_none' => $this->t('- None -')];
    $contexts = $form_state->hasTemporaryValue('gathered_contexts') ? $form_state->getTemporaryValue('gathered_contexts') : [];
    $contexts += $this->contextRepository->getAvailableContexts();

    // When using Layout Builder blocks, make sure the "entity being viewed"
    // context is at first place on the selection list.
    if (isset($contexts['layout_builder.entity'])) {
      /** @var \Drupal\Core\Plugin\Context\EntityContext $entity_context */
      $entity_context = $contexts['layout_builder.entity'];
      $label = new TranslatableMarkup('@entity being viewed', [
        '@entity' => $entity_context->getContextValue()->getEntityType()->getSingularLabel(),
      ]);
      $entity_context->getContextDefinition()->setLabel($label);
      unset($contexts['layout_builder.entity']);
      $contexts = ['layout_builder.entity' => $entity_context] + $contexts;
    }

    foreach ($contexts as $context_id => $context) {
      $data_type = explode(':', $context->getContextDefinition()->getDataType(), 3);
      if (!(reset($data_type) === 'entity') || !($entity_type_id = next($data_type))) {
        continue;
      }
      /** @var \Drupal\Core\Entity\EntityTypeInterface $type_definition */
      $type_definition = $this->entityTypeManager->getDefinition($entity_type_id);
      if ($type_definition instanceof ContentEntityTypeInterface) {
        $options[$context_id] = $context->getContextDefinition()->getLabel();
      }
    }
    return $options;
  }

  /**
   * Settings validation handler.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateSettingsForm(array &$form, FormStateInterface $form_state) {
    $key = ['third_party_settings', 'congruency', 'context'];
    if (!($selected_context = $form_state->getValue($key))) {
      return;
    }
    $allowed = \Drupal::service('congruency.settings_form')->getAllowedContextOptions($form_state);
    if (!array_key_exists($selected_context, $allowed)) {
      $form_state->setError($form['third_party_settings']['congruency']['context'], t('An illegal choice has been detected. Please contact the site administrator.'));
    }
  }

  /**
   * Settings submit handler.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function submitSettingsForm(array &$form, FormStateInterface $form_state) {
    $key = ['third_party_settings', 'congruency', 'context'];
    if (!($selected_context = $form_state->getValue($key))) {
      return;
    }
    if (!($third_party_settings_target = static::getThirdPartySettingsTarget($form_state))) {
      return;
    }
    if ($selected_context === '_none') {
      $third_party_settings_target->unsetThirdPartySetting('congruency', 'context');
    }
    else {
      $third_party_settings_target->setThirdPartySetting('congruency', 'context', $selected_context);
    }
  }

  /**
   * Get the object (target) that holds the congruency third party settings.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The corresponding form state.
   *
   * @return \Drupal\Core\Config\Entity\ThirdPartySettingsInterface|null
   *   The target object, or NULL if not available.
   */
  protected static function getThirdPartySettingsTarget(FormStateInterface $form_state) {
    if ($callback = $form_state->getTemporaryValue('congruency_get_tps_target_callback')) {
      /** @var \Drupal\Core\Config\Entity\ThirdPartySettingsInterface $third_party_settings_target */
      $third_party_settings_target = call_user_func_array($callback, [$form_state]);
      return $third_party_settings_target;
    }
    return NULL;
  }

  /**
   * Callback to get the Layout Builder section component from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\layout_builder\SectionComponent
   *   The section component.
   */
  protected static function getLayoutBuilderSectionComponent(FormStateInterface $form_state) {
    /** @var \Drupal\layout_builder\Form\ConfigureBlockFormBase $form_object */
    $form_object = $form_state->getFormObject();
    return $form_object->getCurrentComponent();
  }

  /**
   * Callback to get the entity from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  protected static function getEntity(FormStateInterface $form_state) {
    /** @var  \Drupal\Core\Entity\EntityFormInterface $form_object */
    $form_object = $form_state->getFormObject();
    return $form_object->getEntity();
  }

}
